# nfcore_eddie

Notes on how to run the Netxflow *nf-core* [RNA-Seq pipeline](https://nf-co.re/rnaseq/3.0)3.0 on Eddie

# Basic setup

How will need to `qlogin` to run Nextflow. You need more than 2G to run nextflow

```
qlogin -l h_vmem=8G
```

Unfortunately you can't submit the initial `nextflow run` process as a job as you can't qsub within a qsub.



### Terminal disconnect.

#### nohup

If your eddie terminal disconnects your nextflow job will stop.

You can run nextflow as a bash script on the command line using [nohup](https://linux.101hacks.com/unix/nohup-command) to prevent this.

e.g.

```
nohup ./nextflow_run.sh &
```

##### screen session

You can also use [Screen](https://linuxize.com/post/how-to-use-linux-screen/) session on a wildwest node to keep the Nextflow process alive if the connection drops.
```
screen -S <session_name>
# reconnect
screen -r <session_name>
```

## Environment Variables

The following environment variables that control the configuration of the Nextflow runtime are needed. Full List [here](https://www.nextflow.io/docs/latest/config.html#environment-variables)

|Name|Description|
|----|-----------|
|NXF_HOME|Nextflow home directory (default: `$HOME/.nextflow`).|
|NXF_TEMP|Directory where temporary files are stored|
|NXF_SINGULARITY_CACHEDIR| Directory where remote Singularity images are stored. When using a computing cluster it must be a shared folder accessible from all computing nodes.|


```
export NXF_HOME=`pwd`/.nextflow
export NXF_TMP="/exports/eddie/scratch/$USER/NXF_TMP"
```

fixme why is this needed?

```
export NXF_PLUGINS_DEFAULT=false
```

### Eddie config

When a pipeline script is launched Nextflow looks for a file named `nextflow.config` in the current directory and in the script base directory (if it is not the same as the current directory). Finally it checks for the file `$NXF_HOME/.nextflow/config`.

When more than one of the above files exist they are merged, so that the settings in the first override the same ones that may appear in the second one, and so on.

The default config file search mechanism can be extended proving an extra configuration file by using the command line option `-c <config file`>.More info [here](https://www.nextflow.io/docs/latest/config.html#)
```
export NXF_CONF="/gpfs/igmmfs01/eddie/bioinfsvice/nextflow_resources/conf/eddie.rnaseq.config"
```



##  Get latest version of Nextflow

The module system had various releases of Nextflow. It doesn't contain the release needed for the nf-core./rnaseq pipeline.

So you will want to download and run the latest [release](https://github.com/nextflow-io/nextflow/releases)

The command below is an examples

```
wget -qO- https://github.com/nextflow-io/nextflow/releases/download/v20.12.0-edge/nextflow |bash
```



## Singularity setup

### Singularity

Singularity is a container engine alternative to Docker. The main advantages of Singularity is that it can be used with unprivileged permissions and doesn’t require a separate daemon process. These, along other features, like for example the support for autofs mounts, makes Singularity a container engine better suited the requirements of HPC workloads. Singularity is able to use existing Docker images, and pull from Docker registries.

```
module load singularity
export NXF_SINGULARITY_CACHEDIR="/exports/igmm/eddie/NextGenResources/nextflow/singularity/nf-core-rnaseq_v3.0"
```

### Singularity environment variables

Singularity will create a directory .singularity in your $HOME directory on eddie this is very small so you will want to

1. Create a symlink to an accessible directory with space.
```
cd $HOME
ln -s /exports/eddie/bioinfsvice/ggrimes2/.singularity .singularity
```

More help on nf-core slack singularity channel https://nfcore.slack.com/archives/CKDDD9JB0

## Test pipeline

Each nf-core pipeline comes with a set of "sensible defaults" for the resource requirements of each of the steps in the workflow, found in config/base.config. These are always loaded and overwritten as needed by subsequent layers of configuration.

Finally, each pipeline comes with a config `profile` called `test` and test_full. These are used for automated pipeline CI tests and will run the workflow with a minimal / full-size public dataset, respectively.



```
./nextflow run nf-core/rnaseq -profile test,singularity -c $NXF_CONF
```





##  RNA-Seq

1. Prepare samplesheet.csv
1. Configure parameters on nf-core RNA-Seq [website](https://nf-co.re/rnaseq)
1. Click launch
1. Copy `nf-params.json` into the directory you run the pipeline from.

### Parameter notes

nf-core use parameters to control the workflow you can specfiy these on the command line
or as a parameter file see nf-core website for full list https://nf-co.re/rnaseq/3.0/usage

or use these helper tool to generate a params file see here https://nf-co.re/launch?id=1615280811_336909f86ffc


```
--skip_trimming=true
--skip_markduplicates=true
--skip_qualimap=true
```

```
./nextflow run nf-core/rnaseq \
-r 3.0 \
-profile singularity \
-params-file nf-params.json \
-c $NXF_CONF
```

 Note you can remove the release option `-r 3.0` to get latest release.


## Known issues

### TMPDIR environment variable on eddie

Nextflow uses the system variable $TMPDIR to execute the process in a temporary folder that is local to the execution node. This variable doesn't seen to be set on eddie. If this variable does not exist, it will create a new temporary directory by using the Linux command `mktemp` which will create a directory in `/tmp` dir of the node.

example
```
cd /tmp
ls nxf*
nxf-17256740582884459472
```

## Pipeline output

Notes [here](https://nf-co.re/rnaseq/3.0/output)

## Notes

### Failed Run

* add the `-resume` option and restart.

It is not unusual to have failed runs. So you may need to do this several times.

### Help

nf-core has an excellent troubleshooting page [here](https://nf-co.re/usage/troubleshooting)
